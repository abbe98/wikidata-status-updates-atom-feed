# Wikidata Status updates - Atom Feed generator

This is a simple webservice which generates an Atom feed of the latest status updates from the [Wikidata Status Updates](https://www.wikidata.org/wiki/Wikidata:Status_updates) page. It does only work for the English version of the page and does not include the actual contents of the updates.

## License

The code is licensed under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
