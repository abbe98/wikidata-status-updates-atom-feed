from flask import Flask, Response
import requests
from bs4 import BeautifulSoup
import re
from datetime import datetime
from xml.etree import ElementTree as ET

app = Flask(__name__)

@app.route("/", methods=["GET"])
def wikidata_feed():
    # Define the URL of the MediaWiki API
    api_url = "https://www.wikidata.org/w/api.php"

    # Specify the parameters for the API request to get the HTML content
    html_params = {
        "action": "parse",
        "format": "json",
        "page": "Wikidata:Status_updates",
        "prop": "text",
    }

    # Make the API request to get the HTML content
    html_response = requests.get(api_url, params=html_params)

    # Parse the JSON response to get the HTML content
    html_data = html_response.json()

    # Check if the query was successful
    if "parse" in html_data and "text" in html_data["parse"]:
        # Get the HTML content from the API response
        html_content = html_data["parse"]["text"]["*"]

        # Parse the HTML content using BeautifulSoup
        soup = BeautifulSoup(html_content, "html.parser")

        # Find the element containing the main contents (adjust as needed)
        main_contents = soup.find("div", class_="mw-parser-output")

        # Extract all links within the main contents
        links = main_contents.find_all("a", href=True)

        # Define a regex pattern for the desired link format
        link_pattern = re.compile(r'^/wiki/Wikidata:Status_updates/(\d{4}_\d{2}_\d{2})$')

        # Create a list to store entry information
        entries = []

        # Iterate through links and add entries to the list
        for link in links:
            href = link["href"]
            match = link_pattern.match(href)
            if match:
                date_str = match.group(1)
                date_object = datetime.strptime(date_str, "%Y_%m_%d")

                entry = {
                    "title": f"Wikidata Status Update - {date_object.strftime('%B %d, %Y')}",
                    "link": f"https://www.wikidata.org{href}",
                    "published": date_object
                }

                entries.append(entry)

        # Sort entries based on the published date
        entries.sort(key=lambda x: x["published"], reverse=True)

        # Create an Atom feed
        feed = ET.Element("feed", xmlns="http://www.w3.org/2005/Atom")

        # Add feed metadata
        title = ET.SubElement(feed, "title")
        title.text = "Wikidata Status Updates Feed"
        link = ET.SubElement(feed, "link", href="https://www.wikidata.org/wiki/Wikidata:Status_updates")
        updated = ET.SubElement(feed, "updated")
        updated.text = datetime.utcnow().isoformat()

        # Add sorted entries to the feed
        for entry in entries:
            entry_xml = ET.SubElement(feed, "entry")

            # Add entry metadata
            entry_title = ET.SubElement(entry_xml, "title")
            entry_title.text = entry["title"]
            entry_link = ET.SubElement(entry_xml, "link", href=entry["link"])
            entry_published = ET.SubElement(entry_xml, "published")
            entry_published.text = entry["published"].isoformat()

        # Convert the XML tree to a string
        feed_xml = ET.tostring(feed, encoding="utf-8").decode("utf-8")

        # Return the Atom feed as an XML response
        return Response(feed_xml, content_type="application/xml")

    else:
        return "Failed to fetch HTML content from the API."

if __name__ == "__main__":
    app.run(debug=False)